How to run application:

* 1) Clone repository.
* 2) Go to 'backend' folder.
* 3) Execute 'yarn install'.
* 4) Go to 'client' folder.
* 5) Execute 'yarn install'.
* 6) Edit .env file to use your personal settings.
* 7) Run mongo db.
* 8) Execute 'yarn start' from 'backend' folder.
* 9) Execute 'yarn start' from 'client' folder.
