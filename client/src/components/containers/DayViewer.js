import React, { Component } from 'react'
import WordLabel from '../containers/WordLabel';

class DayViewer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: "",
            wordsCount: 0,
            isDayExist: false
        }
    }

    updateState(props) {
        const { day } = props
        let newState = day
            ? { isDayExist: true, content: day.content, wordsCount: day.wordsCount }
            : { isDayExist: false, content: "", wordsCount: 0 }
        this.setState(newState)
    }

    componentDidMount() {
        this.updateState(this.props)
    }

    componentWillReceiveProps(props) {
        this.updateState(props)
    }

    render() {
        const { isDayExist, content, wordsCount } = this.state
        return (
            <div>
                {isDayExist
                    ? <div>
                        <div>{content}</div>
                        <br />
                        <WordLabel wordsCount={wordsCount} />
                    </div>
                    : <div><p>This day was not filled.(</p></div>
                }
            </div>

        )
    }
}

export default DayViewer;