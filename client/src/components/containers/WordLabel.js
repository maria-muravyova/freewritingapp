import React, { Component } from 'react'

class WordLabel extends Component {

    render() {
        return (
            <div className="ui label">
                Words count :
                <div className="detail">{this.props.wordsCount}</div>
            </div>
        )
    }
}

export default WordLabel;