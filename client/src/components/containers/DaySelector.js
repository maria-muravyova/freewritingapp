import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import dateFormatter from "../../utils/dateFormatter";
import PreviousMonthButton from "../buttons/PreviousMonthButton";
import NextMonthButton from "../buttons/NextMonthButton";
import MONTH_NAMES from "../../constants";

class DaySelector extends Component {

    constructor(props) {
        super(props);
        this.state = {
            leftVisible: false,
            rightVisible: false,
            days: []
        }
    }

    setInitialData(props) {
        let currentDate = new Date();
        let currentDay = currentDate.getDate();
        let currentMonth = currentDate.getMonth() + 1;
        let currentYear = currentDate.getFullYear();
        let registrationMonth = props.registrationDate.getMonth() + 1;
        let registrationYear = props.registrationDate.getFullYear();
        this.setState({
            currentDay: currentDay,
            currentMonth: currentMonth,
            currentYear: currentYear,
            selectedMonth: props.selectedMonth,
            selectedYear: props.selectedYear,
            leftVisible: this.isLeftButtonVisible(props.selectedYear, registrationYear, props.selectedMonth, registrationMonth),
            rightVisible: this.isRightButtonVisible(props.selectedYear, currentYear, props.selectedMonth, currentMonth),
            days: this.fillDays(props.selectedMonth, props.selectedYear, props.days)
        });
    }

    componentDidMount() {
        this.setInitialData(this.props)
    }

    componentWillReceiveProps(props) {
        this.setInitialData(props)
    }

    createLink(date) {
        return "/day/" + date;
    }

    createClass(status, day, month, year) {
        const { currentYear, currentMonth, currentDay } = this.state;
        const color = this.isSelectedDayLessOrEqualThanCurrent(day, month, year, currentDay, currentMonth, currentYear) ? "green " : "";

        switch (status) {
            case 'Not Started':
                return color + "circle thin icon";
            case 'Not Completed':
                return color + "circle notched icon";
            default:
                return color + "circle icon";
        }
    }

    createDayItem(status, day, month, year) {
        return { status: status, date: dateFormatter(new Date(year, month - 1, day)), day: day, month: month, year: year }
    }

    fillDays(selectedMonth, selectedYear, fetchedDays) {
        let daysCount = this.getDayCount(selectedMonth, selectedYear);

        let days = [];
        for (let i = 1; i <= daysCount; i++) {
            days.push(this.createDayItem('Not Started', i, selectedMonth, selectedYear))
        }

        for (let i = 0; i < fetchedDays.length; i++) {
            let day = fetchedDays[i];
            days[day.day - 1].status = day.status
        }

        return days;
    }

    getDayCount(month, year) {
        if ([1, 3, 5, 7, 8, 10, 12].includes(month)) {
            return 31;
        } else if (month == 2) {
            return this.isYearLeap(year) ? 29 : 28;
        } else {
            return 30;
        }
    }

    isLeftButtonVisible(selectedYear, registrationYear, selectedMonth, registrationMonth) {
        return (selectedYear >= registrationYear) && (selectedMonth > registrationMonth);
    }

    isRightButtonVisible(selectedYear, currentYear, selectedMonth, currentMonth) {
        return (currentMonth !== selectedMonth) || (currentYear !== selectedYear)
    }

    isSelectedDayLessOrEqualThanCurrent(selectedDay, selectedMonth, selectedYear, currentDay, currentMonth, currentYear) {
        return (selectedYear < currentYear
            || (selectedYear === currentYear && selectedMonth < currentMonth)
            || (selectedYear === currentYear && selectedMonth === currentMonth && selectedDay <= currentDay)
        );
    }

    isYearLeap(year) {
        return ((year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0));
    }

    render() {
        const { leftVisible, rightVisible, days, selectedMonth, selectedYear, date, currentDay, currentMonth, currentYear } = this.state;
        const {selectedDate} = this.props
        return (
            <div align="center">
                <div className="ui mini buttons">
                    {leftVisible && <PreviousMonthButton month={selectedMonth} year={selectedYear} />}
                    {
                        days.map(day => (
                            <Link key={day.date}
                                to={this.createLink(day.date)}
                                className={"ui mini " + (day.date === selectedDate ? "" : "basic ") + "icon button"}
                                title={day.date}>
                                <i className={this.createClass(day.status, day.day, day.month, day.year)}></i>
                            </Link>
                        ))
                    }
                    {rightVisible && <NextMonthButton month={selectedMonth} year={selectedYear} />}
                </div>
            </div>
        )
    }
}

export default DaySelector;