import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import MONTH_NAMES from "../../constants";
import dateFormatter from "../../utils/dateFormatter";

class NextMonthButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedYear: props.year,
            selectedMonth: props.month
        }
    }

    createNextMonthLink() {
        const { selectedMonth, selectedYear } = this.state
        let nextMonth = selectedMonth === 12 ? 1 : selectedMonth + 1;
        let nextYear = selectedMonth === 12 ? selectedYear + 1 : selectedYear;
        return "/day/" + dateFormatter(new Date(nextYear, nextMonth - 1, 1));
    }

    getNextMonthText() {
        const { selectedYear, selectedMonth } = this.state;
        let nextMonth = selectedMonth === 12 ? 1 : selectedMonth + 1;
        let nextYear = selectedMonth === 12 ? selectedYear + 1 : selectedYear;
        return MONTH_NAMES[nextMonth - 1] + " " + nextYear;
    }

    render() {
        return (
            <Link to={this.createNextMonthLink()}>
                <button className="ui mini basic icon button" title={this.getNextMonthText()}>
                    <i className="angle right icon"></i>
                </button>
            </Link>
        )
    }
}

export default NextMonthButton;