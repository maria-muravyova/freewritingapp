import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import MONTH_NAMES from "../../constants";
import dateFormatter from "../../utils/dateFormatter";

class PreviousMonthButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedYear: props.year,
            selectedMonth: props.month
        }
    }

    createPreviousMonthLink() {
        const { selectedMonth, selectedYear } = this.state
        let previousMonth = selectedMonth === 1 ? 12 : selectedMonth - 1;
        let previousYear = selectedMonth === 1 ? selectedYear - 1 : selectedYear;
        return "/day/" + dateFormatter(new Date(previousYear, previousMonth - 1, 1));
    }

    getPreviousMonthText() {
        const { selectedYear, selectedMonth } = this.state;
        let previousMonth = selectedMonth === 1 ? 12 : selectedMonth - 1;
        let previousYear = selectedMonth === 1 ? selectedYear - 1 : selectedYear;
        return MONTH_NAMES[previousMonth - 1] + " " + previousYear;
    }

    render() {
        return (
            <Link to={this.createPreviousMonthLink()}>
                <button className="ui mini basic icon button" title={this.getPreviousMonthText()}>
                    <i className="angle left icon"></i>
                </button>
            </Link>
        )
    }
}

export default PreviousMonthButton;