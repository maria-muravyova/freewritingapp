import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ConfirmEmailMessage from '../messages/ConfirmEmailMessage';
import { Link } from "react-router-dom";
import dateFormatter from "../../utils/dateFormatter";

class DashboardPage extends React.Component {

  render() {
    const { isConfirmed } = this.props;
    return (
      <div>
        {!isConfirmed && <ConfirmEmailMessage />}
        <Link to={"/day/" + dateFormatter(new Date())}>Fill today thoughts!</Link>
      </div>
    );
  }
}


DashboardPage.propTypes = {
  isConfirmed: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isConfirmed: !!state.user.confirmed
  }
}

export default connect(mapStateToProps, {})(DashboardPage);
