import React from 'react';
import DayEditor from '../forms/DayEditor';
import DayViewer from '../containers/DayViewer';
import DaySelector from '../containers/DaySelector';
import { getPartial, getSelected, save, update } from '../../actions/days';
import { connect } from 'react-redux';
import { Message, Icon } from 'semantic-ui-react';
import dateFormatter from "../../utils/dateFormatter";
import SuccessfullSaveMessage from '../messages/SuccessfullSaveMessage';
import ErrorMessage from '../messages/ErrorMessage';
import LoadingMessage from '../messages/LoadingMessage';

class DayPage extends React.Component {

    state = {
        viewMode: true,
        paramDate: this.props.match.params.date,
        success: false,
        error: false,
        loading: true
    }

    isSelectedDayLessOrEqualThanCurrent(selectedDay, selectedMonth, selectedYear, currentDay, currentMonth, currentYear) {
        return (selectedYear < currentYear
            || (selectedYear === currentYear && selectedMonth < currentMonth)
            || (selectedYear === currentYear && selectedMonth === currentMonth && selectedDay <= currentDay)
        );
    }

    loadData = (props) => {
        let paramDate = props.match.params.date;
        let selectedDate = new Date(paramDate);
        let currentDate = new Date();
        let selectedDay = selectedDate.getDate();
        let selectedMonth = selectedDate.getMonth() + 1;
        let selectedYear = selectedDate.getFullYear();
        let currentDay = currentDate.getDate();
        let currentMonth = currentDate.getMonth() + 1;
        let currentYear = currentDate.getFullYear()
        if (!this.isSelectedDayLessOrEqualThanCurrent(selectedDay, selectedMonth, selectedYear, currentDay, currentMonth, currentYear)) {
            this.props.history.push("/day/" + dateFormatter(currentDate))
        }
        let newViewMode = paramDate === dateFormatter(currentDate) ? false : true;
        this.setState({ viewMode: newViewMode, paramDate: paramDate, selectedMonth: selectedMonth, selectedYear: selectedYear, loading: true })
        this.props.getPartial({ month: selectedMonth, year: selectedYear }).then(() => {
            this.props.getSelected({ day: selectedDay, month: selectedMonth, year: selectedYear })
                .then(() => this.setState({ loading: false }))
                .catch(() => this.setState({ loading: false }))
            })
            .catch(() => this.setState({ error: true }))

    }

    componentDidMount = () => {
        this.loadData(this.props);
    }

    componentDidUpdate = () => {
        if (this.props.match.params.date !== this.state.paramDate) {
            this.loadData(this.props);
        }
    }

    showSuccessMessage() {
        this.setState({ success: true })
        setTimeout(() => {
            this.setState({ success: false });
        }, 1000);
    }

    showErrorMessage() {
        this.setState({ error: true })
        setTimeout(() => {
            this.setState({ error: false });
        }, 1000);
    }

    submit = (data, isNew) => {
        if (isNew) {
            this.props.save(data).then(() => this.showSuccessMessage())
                .catch(() => this.showErrorMessage())
        } else {
            this.props.update(this.props.day._id, data).then(() => this.showSuccessMessage())
                .catch(() => this.showErrorMessage())
        }
    }

    render() {
        const { viewMode, paramDate, selectedMonth, selectedYear, success, error, loading } = this.state
        return (
            <div>
                {loading && <center><div className="ui active inline loader"></div></center>}
                {!loading && <DaySelector selectedDate={paramDate} selectedMonth={selectedMonth} selectedYear={selectedYear} days={this.props.days} registrationDate={this.props.registrationDate} />}
                {success && <SuccessfullSaveMessage />}
                {error && <ErrorMessage />}
                <h1>{paramDate}</h1>
                {loading && <LoadingMessage />}
                {!loading && (viewMode ? <DayViewer day={this.props.day} /> : <DayEditor submit={this.submit} selectedDate={paramDate} day={this.props.day} />)}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        days: state.days.days,
        day: state.days.selectedDay,
        registrationDate: new Date(state.user.registrationDate)
    }
}

export default connect(mapStateToProps, { getPartial, getSelected, save, update })(DayPage);