import React from 'react';
import { Message, Icon } from 'semantic-ui-react';

const ErrorMessage = () => (
    <Message negative icon>
        <Icon name="warning sign" />
        <Message.Content>
            <Message.Header>
                Error was happened.
            </Message.Header>
        </Message.Content>
    </Message>
);

export default ErrorMessage;