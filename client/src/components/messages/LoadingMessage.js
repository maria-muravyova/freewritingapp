import React from 'react';
import { Message, Icon } from 'semantic-ui-react';

const LoadingMessage = () => (
    <Message icon>
        <Icon name="circle notched" loading />
        <Message.Header>
            Loading current record
        </Message.Header>
    </Message>
);

export default LoadingMessage;