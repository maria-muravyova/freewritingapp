import React from 'react';
import { Message, Icon } from 'semantic-ui-react';

const SuccessfullSaveMessage = () => (
    <Message success icon>
        <Icon name="checkmark" />
        <Message.Content>
            <Message.Header>
                Record was saved succesfully.
            </Message.Header>
        </Message.Content>
    </Message>
);

export default SuccessfullSaveMessage;