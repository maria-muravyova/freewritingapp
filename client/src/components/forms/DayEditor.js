import React, { Component } from 'react'
import WordLabel from '../containers/WordLabel';
import { Form } from "semantic-ui-react";
import wordCounter from "../../utils/wordCounter";

class DayEditor extends Component {

    state = {
        content: "",
        wordsCount: 0,
        isNew: true
    }

    setInitialData(props) {
        let newState = props.day ? { content: props.day.content, wordsCount: props.day.wordsCount, isNew: false } : { isNew: true };
        this.setState(newState)
    }

    componentDidMount() {
        this.setInitialData(this.props)
    }

    componentWillReceiveProps(props) {
        this.setInitialData(props)
    }

    onChange = e =>
        this.setState({
            content: e.target.value,
            wordsCount: wordCounter(e.target.value)
        });

    onSubmit = () => {
        this.props.submit({ content: this.state.content, wordsCount: this.state.wordsCount, date: this.props.selectedDate }, this.state.isNew)
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.onSubmit} >
                    <div className="field">
                        <button className="ui button">
                            <i className="icon save"></i>
                            Save
                        </button>
                    </div>
                    <div className="field">
                        <textarea onChange={this.onChange} value={this.state.content} style={{ overflow: "auto", resize: "none" }} placeholder="Write your thoughts..." rows="25"></textarea>
                    </div>
                </Form>
                <br />
                <WordLabel wordsCount={this.state.wordsCount} />
            </div>

        )
    }
}

export default DayEditor;