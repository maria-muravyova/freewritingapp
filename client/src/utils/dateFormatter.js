export default (date) => {
    let curr_day = date.getDate();
    let curr_month = date.getMonth() + 1;
    let curr_year = date.getFullYear();
    return curr_year + "-" + (curr_month < 10 ? "0" + curr_month : curr_month) + "-" + (curr_day < 10 ? "0" + curr_day : curr_day);
};