export default (text) => {
    let words = text.match(/([\wА-Яа-я]+)/g);
    let count = words == null ? 0 : words.length;
    return count;
}