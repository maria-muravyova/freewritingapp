import { combineReducers } from "redux";
import user from "./reducers/user";
import days from "./reducers/days";

export default combineReducers({
  user,
  days
});