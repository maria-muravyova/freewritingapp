import axios from 'axios';

export default {
    user: {
        login: credentials =>
            axios.post("/api/auth", { credentials }).then(res => res.data.user),
        signup: user =>
            axios.post("/api/users", { user }).then(res => res.data.user),
        confirm: token =>
            axios.post("/api/auth/confirmation", { token })
                .then(res => res.data.user),
        resetPasswordRequest: email =>
            axios.post("/api/auth/reset_password_request", { email }),
        validateToken: token =>
            axios.post("/api/auth/validate_token", { token }),
        resetPassword: data => axios.post('/api/auth/reset_password', { data })
    },
    days: {
        getPartial: data =>
            axios.get("/api/days/" + data.year + "/" + data.month + "?fields=_id,status,day,month,year").then(res => res.data.days),
        getSelectedDay: data =>
            axios.get("/api/days/" + data.year + "/" + data.month + "/" + data.day).then(res => res.data.day),
        save: data =>
            axios.post("/api/days", { data }).then(res => res.data.day),
        update: (id, data) =>
            axios.put("/api/days/" + id, { data }).then(res => res.data.day)
    }
}