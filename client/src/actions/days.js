import api from '../api';
import { PARTIAL_DAYS_FETCHED, SELECTED_DAY_FETCHED, DAY_SAVED, DAY_UPDATED } from '../types';

export const getPartial = data => dispatch =>
    api.days.getPartial(data)
        .then(days => { dispatch({type: PARTIAL_DAYS_FETCHED, days} );
});

export const getSelected = data => dispatch =>
    api.days.getSelectedDay(data)
        .then(day => { dispatch({type: SELECTED_DAY_FETCHED, day})})
        .catch(dispatch({type: SELECTED_DAY_FETCHED, day: null}))

export const save = data => dispatch => 
    api.days.save(data)
        .then(day => { dispatch({ type: DAY_SAVED, day })})

export const update = (id, data) => dispatch => 
    api.days.update(id, data)
        .then(day => dispatch({type: DAY_UPDATED, day }))