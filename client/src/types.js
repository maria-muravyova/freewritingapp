export const USER_LOGGED_IN = 'USER_LOGGED_IN';
export const USER_LOGGED_OUT = 'USER_LOGGED_OUT';
export const PARTIAL_DAYS_FETCHED = 'PARTIAL_DAYS_FETCHED';
export const SELECTED_DAY_FETCHED = 'SELECTED_DAY_FETCHED';
export const DAY_SAVED = 'DAY_SAVED';
export const DAY_UPDATED = 'DAY_UPDATED';