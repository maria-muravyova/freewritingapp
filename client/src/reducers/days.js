import { PARTIAL_DAYS_FETCHED, SELECTED_DAY_FETCHED, DAY_SAVED, DAY_UPDATED } from '../types';

export default function days(state = { days: [], selectedDay: null }, action = {}) {
    switch (action.type) {
        case PARTIAL_DAYS_FETCHED:
            return { ...state, days: action.days }
        case SELECTED_DAY_FETCHED:
            return { ...state, selectedDay: action.day }
        case DAY_SAVED:
        case DAY_UPDATED:
            return { ...state, selectedDay: action.day }
        default:
            return state;
    }
}