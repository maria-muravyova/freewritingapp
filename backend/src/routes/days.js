import express from 'express'
import Day from '../models/Day';
import parseErrors from '../utils/parseErrors';
import authenticate from '../middlewares/authenticate';
import filterByFields from '../utils/filterByFields';

const router = express.Router();
router.use(authenticate);

function update(res, day, newDay) {
    if (day.content !== newDay.content) {
        day.content = newDay.content;
    }
  
    if (day.wordsCount !== newDay.wordsCount) {
        day.wordsCount = newDay.wordsCount;
    }

    day.setStatus();
    day.save()
      .then(updatedDay => res.json({day:updatedDay}))
      .catch(err => res.status(400).json({ errors: parseErrors(err.errors)}))
}

/** Creates day record for current user */
router.post('/', (req, res) => {
        const { date, wordsCount, content } = req.body.data;
        const day = new Day({wordsCount: wordsCount, content: content, userId: req.currentUser._id})
        day.setDate(date)
        day.setStatus()
        day.save().then(day => res.json({day}))
    });

/** Returns all records for current user.*/
router.get('/', (req, res) => {
    Day.find({userId: req.currentUser._id})
        .then(days => res.json({days}));
});

/** Returns all records for current user for the specified month.*/
router.get('/:year/:month', (req, res) => {
    let year = +req.params.year;
    let month = +req.params.month;
    Day.find({
        userId: req.currentUser._id,
        year: year,
        month: month
    }).then(days => {
        if (req.query.fields) {
            days = days.map(day => filterByFields(req.query.fields, day))
        }
        res.json({days});
    });
});

/** Returns day record for current user by specified date. */
router.get('/:year/:month/:day', (req, res) => {
    let year = +req.params.year;
    let month = +req.params.month;
    let day = +req.params.day
    Day.findOne({
        userId: req.currentUser._id,
        year: year,
        month: month,
        day: day
    }).then(day => day  
        ? res.json({day})
        : res.status(400).json({errors: {global: "Record for this day was not found."}})
    );
});

/** Update record with specified id. */
router.put('/:_id', (req, res) => {
    Day.findOne({_id: req.params._id})
    .then(day => day 
        ? update(res, day, req.body.data)
        : res.status(400).json({errors: {global: "Record is not found."}})
    )
});

/** Delete record with specified id. */
router.delete('/:_id', (req, res) => {
    Day.remove({ _id: req.params._id }, (err, message) => {
        if (err) { 
            res.status(500).json({ errors: { global: err }})
        }
        res.json({message: message});
    })
});

export default router;