export default function(fields, data) {
    let fieldsArr = fields.split(',');
    let filteredResult = {};
    for (let index in fieldsArr) {
        let field = fieldsArr[index];
        if (data[field]) {
            filteredResult[field] = data[field];
        } 
    }
    return filteredResult;
}