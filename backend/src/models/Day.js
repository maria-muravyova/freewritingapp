import mongoose from "mongoose";

const WORD_COUNT_NORM = 750;

const schema = new mongoose.Schema({
    content: { type: String, required: true },
    wordsCount: {type: Number, default: 0, required: true},  
    userId: {type: mongoose.Schema.Types.ObjectId, required: true },
    date: {type: Date, default: new Date(), required: true },
    year: {type: Number, default: new Date().getFullYear(), required: true },
    month: {type: Number, default: new Date().getMonth() + 1, required: true },
    day: {type: Number, default: new Date().getDate(), required: true },
    status: {
        type: String, 
        enum: ['Not Started', 'Not Completed', 'Completed'], 
        default: 'Not Started'
    }
});

schema.methods.setDate = function setDate(date) {
    if (date) {
        this.date = date;
        this.year = this.date.getFullYear();
        this.month = this.date.getMonth() + 1;
        this.day = this.date.getDate();
    }
}

schema.methods.setStatus = function setStatus() {
    if (this.wordsCount == 0) {
        this.status = "Not Started";
    } else if (this.wordsCount < WORD_COUNT_NORM) {
        this.status = "Not Completed";
    } else {
        this.status = "Completed";
    }
}

export default mongoose.model("Day", schema);